import csv
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


class Usecases:
    def usecase1(data):
        for record in data:
            print(record)

    def usecase2(data):
        print(tuple(data))

    def usecase3(data):
        with open("file.txt", 'w') as output:
            for row in data:
                output.write(str(row) + '\n')

    def usecase4(content):
        content_list = content.split("\n")
        print(content_list)

    def usecase5(data):
        csv_columns = ['Name', 'population', 'Rank']
        csv_file = "State_details.csv"
        try:
            with open(csv_file, 'w') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
                writer.writeheader()
                for record in data:
                    writer.writerow(record)
        except IOError:
            print("I/O error")

    @staticmethod
    def usecase6():
        input1 = int(input("enter choice: 1:add 2:subtract 3:muliply 4:divide"))
        if (input1 == 1):
            value1 = int(input("enter value1"))
            value2 = int(input("enter value2"))
            print(value1 + value2)
        elif (input1 == 2):
            value1 = int(input("enter value1"))
            value2 = int(input("enter value2"))
            print(value1 - value2)
        elif (input1 == 3):
            value1 = int(input("enter value1"))
            value2 = int(input("enter value2"))
            print(value1 * value2)
        elif (input1 == 4):
            numerator = int(input("enter numnerator"))
            denominator = int(input("rnyter denominator"))
            print(numerator / denominator)
        else:
            print("Invalid input")

    @staticmethod
    def usecase7():

        recipients = "sireesha.k@knowledgelens.com"
        body = 'this consist of csv file'
        msg = MIMEMultipart()
        msg['Subject'] = 'Message in Python'
        msg['From'] = 'sireesha.k@knowledgelens.com'
        msg['To'] = ', '.join(recipients.split(','))

        msg.attach(MIMEText(body, 'plain'))
        with open("State_details.csv", 'r') as file:
            msg.attach(MIMEApplication(file.read(), Name="State_details"))

        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login('sireesha.k@knowledgelens.com', 'Sireeshaks@11')
        server.send_message(msg)
        server.quit()
